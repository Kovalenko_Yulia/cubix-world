(function($){
$(document).ready(function(){

    //bubbles
    var speed = 350;
    var timer = setInterval(showItem, speed);
    var items =  $('#parallax .layer');
    var length = items.length;
    var index = 0;
    function showItem() {
         items.eq(index).addClass('displayItem');
         index++;
         if (index >= length) {
             clearInterval(timer);
         }
    }

    var bArray = [];
    var sArray = [4,6,8,10,12,15,20];
    for (var i = 0; i < $('.bubbles').width(); i++) {
        bArray.push(i);
    }
    function randomValue(arr) {
        return arr[Math.floor(Math.random() * arr.length)];
    }
    setInterval(function(){

        var size = randomValue(sArray);
        $('.bubbles').append('<div class="individual-bubble" style="left: ' + randomValue(bArray) + 'px; width: ' + size + 'px; height:' + size + 'px;"></div>');

        $('.individual-bubble').animate({
            'bottom': '100%',
            'opacity' : '-=0.7'
        }, 3000, function(){
            $(this).remove()
        }
        );
    }, 350);


    //slider
    $('.slickSlider').slick({
    });

    //background parallax movement
    var lFollowX = 0,
    lFollowY = 0,
    x = 0,
    y = 0,
    friction = 1 / 30;

    function moveBackground() {
        x += (lFollowX - x) * friction;
        y += (lFollowY - y) * friction;

        translate = 'translate(' + x + 'px, ' + y + 'px)';

        $('#parallax .move').each(function(){
            if ($(this).hasClass('birds')){
                $(this).css({
                    '-webit-transform': 'translate(' + x*0.5 + 'px, ' + y*0.5 + 'px)',
                    '-moz-transform': 'translate(' + x*0.5 + 'px, ' + y*0.5 + 'px)',
                    'transform': 'translate(' + x*0.5 + 'px, ' + y*0.5 + 'px)'
                });
            } else if ($(this).hasClass('cubes')){
                $(this).css({
                    '-webit-transform': 'translate(' + x*2 + 'px, ' + y*2 + 'px)',
                    '-moz-transform': 'translate(' + x*2 + 'px, ' + y*2 + 'px)',
                    'transform': 'translate(' + x*2 + 'px, ' + y*2 + 'px)'
                });
            } else if ($(this).hasClass('cube-left')){
                $(this).css({
                    '-webit-transform': 'translate(' + x*1.2 + 'px, ' + y*3 + 'px)',
                    '-moz-transform': 'translate(' + x*1.2 + 'px, ' + y*3 + 'px)',
                    'transform': 'translate(' + x*1.2 + 'px, ' + y*3 + 'px)'
                });
            } else if ($(this).hasClass('cube-right')){
                $(this).css({
                    '-webit-transform': 'translate(' + -x*1.2 + 'px, ' + y*3 + 'px)',
                    '-moz-transform': 'translate(' + -x*1.2 + 'px, ' + y*3 + 'px)',
                    'transform': 'translate(' + -x*1.2 + 'px, ' + y*3 + 'px)'
                });
            } else{
                $(this).css({
                    '-webit-transform': translate,
                    '-moz-transform': translate,
                    'transform': translate
                });
            }
        });

        window.requestAnimationFrame(moveBackground);
    }

    $(window).on('mousemove click', function(e) {

        var lMouseX = Math.max(-100, Math.min(100, $(window).width() / 2 - e.clientX));
        var lMouseY = Math.max(-100, Math.min(100, $(window).height() / 2 - e.clientY));
        lFollowX = (20 * lMouseX) / 100; // 100 : 12 = lMouxeX : lFollow
        lFollowY = (10 * lMouseY) / 100;

    });

    moveBackground();

    //stop parallax if scrolled out of view
    $(window).scroll(function() {
        if($(this).scrollTop() >= 450) {
            $('#parallax .layer').each(function(){
                if ($(this).hasClass('birds')||$(this).hasClass('cubes')||$(this).hasClass('cube-left')||$(this).hasClass('cube-right')){
                    $(this).removeClass('move');
                }
            });
        } else {
            $('#parallax .layer').each(function(){
                if ($(this).hasClass('birds')||$(this).hasClass('cubes')||$(this).hasClass('cube-left')||$(this).hasClass('cube-right')){
                    $(this).addClass('move');
                }
            });
        }
    });

    //sidebar progress bar animation
    var totalScore = 0;
    $('.sidebar .monitoring ul li').each(function(){
        var item = $(this).find('.text span').html();
        totalScore = totalScore + +item;

        $(this).find('.bar .water').css('width', item + '%');
    });
    $('.sidebar .monitoring .score span').first().html(totalScore);
    $('.sidebar .monitoring .score span.total').html($('.sidebar .monitoring ul li').length*100);


    //autoplay video
    $(document).on('click','.js-videoPoster',function(e) {
        e.preventDefault();
        var poster = $(this);
        var wrapper = poster.closest('.js-videoWrapper');
        videoPlay(wrapper);
    });

    function videoPlay(wrapper) {
        var iframe = wrapper.find('.js-videoIframe');
        var src = iframe.data('src');
        wrapper.addClass('videoWrapperActive');
        iframe.attr('src',src);
    }

    //curved letters at Information page
    $('p.curved').elipText({radius: 70});

    //make Submit button hover effect on h5 hover
    $('h5.exchange-button').mouseover(function(){
        $(this).siblings('input').addClass('exchange-button-hover');
    });
    $('h5.exchange-button').mouseout(function(){
        $(this).siblings('input').removeClass('exchange-button-hover');
    });

    //submit form on h5 click
    $('h5.exchange-button').on('click', function(){
        $(this).parents('form').submit();
    });

    $('select').niceSelect();

});

})(jQuery);